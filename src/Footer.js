import React, { Component } from 'react';
import { Icon } from 'semantic-ui-react';
import injectSheet from 'react-jss';

const styles = {
    footer: {
        width: "100%",
        clear: 'both',
        position: 'absolute',
        height: '100px',
        bottom: 0,
    },
    content: {
        width: '100%',
        height: '100%',
        padding: '20px',
        display: 'flex',
        flexFlow: 'row nowrap',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    icon: {
        padding: '0 25px'
    }

}
class Footer extends Component {
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.footer}>
                <div className={classes.content}>
                    <div>
                        <Icon name="instagram" size="big" color="purple" className={classes.icon} />
                        {/*<Icon name="facebook" size="big" color="violet" className={classes.icon} />*/}
                        <Icon name="facebook messenger" size="big" color="blue" className={classes.icon} />
                        <Icon name="mail" size="big" color="red" className={classes.icon} />
                    </div>
                </div>
            </div>
        );
    }
}

export default injectSheet(styles)(Footer);
