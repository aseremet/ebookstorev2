import React, { Component } from 'react';
import { Image, Card } from 'semantic-ui-react';
import injectSheet from 'react-jss';
import { Link } from 'react-router-dom';

const styles = {
    card: {
        width: '350px !important',
        height: '400px',
    }
}
class Item extends Component {
    render() {
        const { photo, name, link, classes } = this.props;
        return (
            <Card as={Link} to={link} className={classes.card}>
                <Card.Content textAlign="center">
                    <Card.Header>{name.toUpperCase()}</Card.Header>
                </Card.Content>
                <Image src={process.env.PUBLIC_URL+'/photos/'+photo+'.jpg'} wrapped ui={false} />
            </Card>
        );
    }
}

export default injectSheet(styles)(Item);
