import React, { Fragment } from 'react';
import HomePage from './HomePage';
import Header from './Header';
import Footer from './Footer';
import './App.css';
import { useMediaQuery } from 'react-responsive';
import injectStyle from 'react-jss';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { createBrowserHistory } from "history";
import Diet from './Diet';
import Cart from './Cart';
import MyProducts from './MyProducts';

const styles = {
  mainPage: {
    position: 'relative',
    minHeight: '100vh',
  }
}


function App(classes) {
  const history = createBrowserHistory();
  const mobile = useMediaQuery({ maxWidth: 730 })
  return (
    <Router history={history}>
      <Switch>
        <Fragment>
          <div className={classes.classes.mainPage}>
            <Header mobile={mobile} />
            <Route exact path="/">
              <HomePage mobile={mobile} />
            </Route>
            <Route path="/dieta" component={Diet} />
            <Route path="/koszyk" component={Cart} />
            <Route path="/moje-ebooki" component={MyProducts} />
            <Footer />
          </div>
        </Fragment>
      </Switch>
    </Router>
  );
}

export default injectStyle(styles)(App);
