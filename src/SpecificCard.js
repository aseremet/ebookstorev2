import React, { Component } from 'react';
import { Image, Card } from 'semantic-ui-react';
import injectSheet from 'react-jss';

const styles = {
    card: {
        width: '300px !important',
    }
}
class Item extends Component {
    render() {
        const { /*photo,*/ topic, goal, classes } = this.props;
        return (
            <Card href="#" className={classes.card}>
                <Card.Content>
                    <Card.Header>{goal.toUpperCase()}</Card.Header>
                    <Card.Meta>
                        <span>{topic}</span>
                    </Card.Meta>
                    <Card.Description>
                        Matthew is a musician living in Nashville.
                    </Card.Description>
                </Card.Content>
                <Image src={'https://react.semantic-ui.com/images/avatar/large/matthew.png'} wrapped ui={false} />
            </Card>
        );
    }
}

export default injectSheet(styles)(Item);
