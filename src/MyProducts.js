import React, { Component } from 'react';
import { Table, Icon } from 'semantic-ui-react';
import injectSheet from 'react-jss';
import { PDFDocument, rgb } from 'pdf-lib';
import downloadjs from 'downloadjs';

const issues = [
    {id: 12, title: "Dieta lowcarb 1700kcal", link: "lowcarb-1700.pdf", price: 140},
    {id: 23, title: "Dieta antyzapalna 2000kcal", link: "antyzapalna-2000.pdf", price: 170},
    {id: 3, title: "Dieta na masę 2200kcal", link: "masa-2200.pdf", price: 120},
    {id: 23, title: "Dieta redukcyjna 1600kcal", link: "redu-1600.pdf", price: 110},
];
const styles = {
  content: {
    width: '100%',
    padding: '20px',
    paddingBottom: '100px',
  }, 
  text: {
    fontFamily: 'Arial',
    fontSize: "20px",
    textAlign: "center",
    padding: "60px 0 100px 0",
  }
}

async function downloadPDF(link, userId) {
  const url = process.env.PUBLIC_URL+"/pdfs/"+link;
  const existingPdfBytes = await fetch(url).then(res => res.arrayBuffer())
  const pdfDoc = await PDFDocument.load(existingPdfBytes)
  const pages = pdfDoc.getPages()
  pages.map((page)=>{
    const { width, height } = page.getSize()
    page.drawText(userId, {
      x: Math.floor(Math.random() * (width-55)) + 5,
      y: Math.floor(Math.random() * (height-10)) + 5,
      size: 0.1,
      color: rgb(1, 1, 1),
    })
  })
  const pdfBytes = await pdfDoc.save()
  downloadjs(pdfBytes, link, "application/pdf");
};

class MyProducts extends Component {
  userId = "12345678user123"

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.content}>
        <div className={classes.text}>E-booki dostępne do pobrania:</div>
        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell></Table.HeaderCell>
              <Table.HeaderCell>Tytuł</Table.HeaderCell>
              <Table.HeaderCell>Cena</Table.HeaderCell>
              <Table.HeaderCell>Pobierz</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {issues.map((issue, i) => <Table.Row>
              <Table.Cell>{i+1}</Table.Cell>
              <Table.Cell>{issue.title}</Table.Cell>
              <Table.Cell>{issue.price}</Table.Cell>
              <Table.Cell>
                <Icon name='download' onClick={() => downloadPDF(issue.link, this.userId)} />
              </Table.Cell>
            </Table.Row>)}
          </Table.Body>
        </Table>
      </div>
    );
  }
}

export default injectSheet(styles)(MyProducts);
