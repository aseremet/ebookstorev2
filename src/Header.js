import React, { Component } from 'react';
import { Menu, Image } from 'semantic-ui-react';
import injectSheet from 'react-jss';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

const styles = {
    header: {
        width: "100%",
        padding: "20px 30px",
        display: "flex",
        flexFlow: "row wrap",
        justifyContent: "space-between",
        alignItems: "center",
    },
    name: {
        display: "flex",
        flexFlow: "row nowrap",
        alignItems: "center",
        fontFamily: "Sonnam",
        fontSize: "60px",
    },
    logo: {
        marginRight: "30px",
    },
    text: {
        paddingBottom: "10px",
    },
    menu: {
    }
}
class Header2 extends Component {
    state = { activeItem: '' }
    handleItemClick = (e, { name }) => this.setState({ activeItem: name });
    render() {
        const { classes, mobile } = this.props;
        return (
            <div className={classes.header} height={mobile ? 500 : 300}>
                <div className={classes.name}>
                    <Image src="logo.png" size="tiny" className={classes.logo} />
                    <div className={classes.text}>ebookstore</div>
                </div>
                <Menu secondary size='small' color="red" floated="right" compact className={classes.menu}>
                    <Menu.Item
                        as={Link}
                        to="/"
                        name=''
                        icon='home'
                        active={window.location.pathname === '/'}
                        onClick={this.handleItemClick}
                    />
                    <Menu.Item
                        as={Link}
                        to="/dieta"
                        name='dieta'
                        active={window.location.pathname === '/dieta'}
                        onClick={this.handleItemClick}
                    />
                    <Menu.Item
                        as={Link}
                        to="/trening"
                        name='trening'
                        active={window.location.pathname === '/trening'}
                        onClick={this.handleItemClick}
                    />
                    {/* <Menu.Menu position='right'>
                        <Menu.Item>
                            <Button primary>Zaloguj się</Button>
                        </Menu.Item>
                    </Menu.Menu> */}
                    <Menu.Item
                        as={Link}
                        to="/koszyk"
                        name=''
                        icon='cart'
                        active={window.location.pathname === '/koszyk'}
                        onClick={this.handleItemClick}
                    />
                    <Menu.Item
                        as={Link}
                        to="/moje-ebooki"
                        name='Moje ebooki'
                        active={window.location.pathname === '/moje-ebooki'}
                        onClick={this.handleItemClick}
                    />
                </Menu>
            </div>
        );
    }
}

export default injectSheet(styles)(withRouter(Header2));
