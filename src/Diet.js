import React, { Component } from 'react';
import { Card } from 'semantic-ui-react';
import Item from './SpecificCard';
import injectSheet from 'react-jss';

const styles = {
  content: {
    width: '100%',
    padding: '20px',
    paddingBottom: '100px',
  }, 
  text: {
    fontFamily: 'Arial',
    fontSize: "20px",
    textAlign: "center",
    padding: "60px 0 100px 0",
  }
}
class Home extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.content}>
          <Card.Group centered doubling stackable className={classes.content}>
          <Item photo="diet" topic="1700kcal" goal="redukcja" />
          <Item photo="workout" topic="2200kcal" goal="redukcja/masa" />
          <Item photo="dietandworkout" topic="3000kcal" goal="masa" />
          <Item photo="workout" topic="Idywidualne zapotrzebowanie" goal="redukcja/masa" />
        </Card.Group>
      </div>
    );
  }
}

export default injectSheet(styles)(Home);
