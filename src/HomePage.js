import React, { Component } from 'react';
import { Card } from 'semantic-ui-react';
import Item from './Card';
import injectSheet from 'react-jss';

const styles = {
  content: {
    width: '100%',
    padding: '20px',
    paddingBottom: '100px',
  }, 
  text: {
    fontFamily: 'Arial',
    fontSize: "20px",
    textAlign: "center",
    padding: "60px 0 100px 0",
  }
}
class Home extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.content}>
        <Card.Group centered doubling stackable className={classes.content}>
          <Item link="/dieta" photo="diet" name="Dieta" />
          <Item link="/trening" photo="workout" name="Trening" />
        </Card.Group>
      </div>
    );
  }
}

export default injectSheet(styles)(Home);
