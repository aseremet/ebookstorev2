import React, { Component } from 'react';
import injectSheet from 'react-jss';

const cart = [
    {id: 12, name: "Dieta lowcarb 1700kcal", link: "lowcarb-1700.pdf", price: 140},
    {id: 23, name: "Dieta antyzapalna 2000kcal", link: "antyzapalna-2000.pdf", price: 170},
];
const styles = {
  content: {
    width: '100%',
    padding: '20px',
    paddingBottom: '100px',
  }, 
  text: {
    fontFamily: 'Arial',
    fontSize: "20px",
    textAlign: "center",
    padding: "60px 0 100px 0",
  }
}
class Cart extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.content}>
        <div className={classes.text}>Koszyk</div>
        
      </div>
    );
  }
}

export default injectSheet(styles)(Cart);
